# Apriori: Associative Rule Learning
# Aprioy.py is the library: collection of classes with methods 
# Data: Transactions from customers represensted in rows

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Data Preprocessing, note it is important that the data should not have an header and must be a list of list
dataset = pd.read_csv('Market_Basket_Optimisation.csv', header = None)
# converting the dataframe into list
# Row: 0 - 7501 and 0, 20: columns and must be string datatype: str
# converts dataframe into list of list and must be string
transactions = []
for i in range(0, 7501):
    transactions.append([str(dataset.values[i,j]) for j in range(0, 20)])

# Training Apriori on the dataset
from apyori import apriori
#min_lenght is the maximum number of products we want to associate 
# min_support = (3 * 7) / 7501
# min_confidence 0.3 based on intuition
rules = apriori(transactions, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2)

# Visualising the results
results = list(rules)


