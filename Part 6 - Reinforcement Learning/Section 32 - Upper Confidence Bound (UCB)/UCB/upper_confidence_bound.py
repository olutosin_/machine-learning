# Upper Confidence Bound

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Ads_CTR_Optimisation.csv')

# Implementing UCB
import math
N = 10000   # Total number of rounds
d = 10      # Total number of Ads
ads_selected = []  # Holds the Ads selected
# Creates a vector of size d containing inly zeroes
numbers_of_selections = [0] * d
# Creates a vector of size d containing zeroes
sums_of_rewards = [0] * d
total_reward = 0
for n in range(0, N):         # Loops through number of rounds
    ad = 0
    max_upper_bound = 0       # To clear the upper bound obtained since it will be different in each round
    for i in range(0, d):    # Loops through the Ads
        if (numbers_of_selections[i] > 0):  # This means if this an Ad is selected ones then we can calculate the reward
            average_reward = sums_of_rewards[i] / numbers_of_selections[i]
            delta_i = math.sqrt(3/2 * math.log(n + 1) / numbers_of_selections[i])  # n + 1 was added in other to take care of index in python that starts from zero
            upper_bound = average_reward + delta_i
        else:
            upper_bound = 1e400  #10 raised to power of 400. This was made high in other to allow the 10 Ads to run before exploitation
        if upper_bound > max_upper_bound:
            max_upper_bound = upper_bound
            ad = i   # This is to track the Ad with the maximum upper bound
    ads_selected.append(ad)  # Append the Ad
    numbers_of_selections[ad] = numbers_of_selections[ad] + 1  # Increments the selection
    reward = dataset.values[n, ad]  # Real reward
    sums_of_rewards[ad] = sums_of_rewards[ad] + reward  # Sum of reward of the Ad selected 
    total_reward = total_reward + reward     # Updating sum of rewards
  
# Visualising the results
plt.hist(ads_selected, color = "red", bins = 25)
plt.title('Histogram of ads selections')
plt.xlabel('Ads')
plt.ylabel('Number of times each ad was selected')
plt.show()