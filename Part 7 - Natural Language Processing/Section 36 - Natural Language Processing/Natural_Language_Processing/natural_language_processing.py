# Natural Language Processing
# Dataset: Reviews from the customers of a restaurant
# For NLP we can use DTC, RFC or Naive Bayes

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
# Quoting of value 3 ensures the double quotes are ignored
dataset = pd.read_csv('Restaurant_Reviews.tsv', delimiter = '\t', quoting = 3)



# Cleaning the texts
import re      
import nltk
nltk.download('stopwords')         # Holds the list of unwanted words
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
corpus = []
for i in range(0, 1000):
    # Removes all characters from the text but we can use a trick by specifying all the letters we wanted which follows the hat sign
    # ' ' the renoved character will be replaced by space 
    review = re.sub('[^a-zA-Z]', ' ', dataset['Review'][i])   
    review = review.lower()  # return lowercase characters
    review = review.split()  # convert the string into its different words
    ps = PorterStemmer()     # Creating an object of stem
    review = [ps.stem(word) for word in review if not word in set(stopwords.words('english'))]
    review = ' '.join(review)
    corpus.append(review)

# Creating the Bag of Words model 
# Tokenization
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer(max_features = 1500)   # This comes from the number of columnns created
X = cv.fit_transform(corpus).toarray()
y = dataset.iloc[:, 1].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 0)

# Fitting Naive Bayes to the Training set
from sklearn.naive_bayes import GaussianNB
classifier = GaussianNB()
classifier.fit(X_train, y_train) 

# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)