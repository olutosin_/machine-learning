"""

@ Developer: Young Olutosin
@Brief     : Program to predict SNR using RSSI and RSRP
@Comment   : The accuracy given by the model is low. More dataset will be needed. Also 
             how the dataset was obtained is something to look into.
@Date      : 5-10-2019
         
"""

# Polynomial Regression

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('data.csv')
X = dataset.iloc[:, 1:].values
y = dataset.iloc[:, 0].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree = 4)
X_poly = poly_reg.fit_transform(X_train)
X_poly_test = poly_reg.fit_transform(X_test)

y_train = y_train.reshape(-1,1)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_Y = StandardScaler()

X_train = sc_X.fit_transform(X_poly)
X_test = sc_X.transform(X_poly_test)


y_train = sc_Y.fit_transform(y_train)



# Fitting Linear Regression to the dataset
from sklearn.linear_model import LinearRegression


# Fitting Polynomial Regression to the dataset

poly_lin_reg = LinearRegression()
poly_lin_reg.fit(X_train, y_train)


# Predicting a new result with Polynomial Regression
y_pred = poly_lin_reg.predict(X_test)
y_pred_weight = sc_Y.inverse_transform(y_pred)

y_pred_weight_int = y_pred_weight.astype(int)
y_pred_weight_int = y_pred_weight_int.reshape(69,)

Predict = ( y_pred_weight_int == y_test)

TruePredict = sum(Predict)
FalsePredict = len(y_test) - TruePredict
Accuracy = (TruePredict/(len(y_test)) * 100)
print("Accuracy is " + format(Accuracy, "2.2f") + "%")

# End



