"""

@ Developer: Young Olutosin
@Brief     : Program to predict SNR using RSSI and RSRP
@Comment   : The accuracy given by the model is low. More dataset will be needed. Also 
             how the dataset was obtained is something to look into.
@Date      : 5-10-2019
            
"""

# Random Forest Regression

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('data.csv')
X = dataset.iloc[:, 1:].values
y = dataset.iloc[:, 0].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

y_train = y_train.reshape(-1,1)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_Y = StandardScaler()

X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

y_train = sc_Y.fit_transform(y_train)


# Fitting Random Forest Regression to the dataset
from sklearn.ensemble import RandomForestRegressor
regressor = RandomForestRegressor(n_estimators = 10, random_state = 0)
regressor.fit(X_train, y_train)

# Predicting a new result
y_pred = regressor.predict(X_test)
y_pred_weight = sc_Y.inverse_transform(y_pred)
y_pred_weight_conv  = y_pred_weight.astype(int)

Predict = (y_test == y_pred_weight_conv)
TruePredict = sum(Predict)
FalsePredict = len(y_test) - TruePredict
Accuracy = (TruePredict/(len(y_test)) * 100)
print("Accuracy of this model is " + format(Accuracy, "2.2f") + "%")

# End




